% !TEX program = xelatex
% !TEX encoding = UTF-8 Unicode

\NeedsTeXFormat{LaTeX2e}

\RequirePackage{pgfkeys}
\RequirePackage{pgfopts}

\pgfkeys{%
	/gpppi/.is family,
	/gpppi,
	relativepath/.store in=\relativepath,
	relativepath={./},
	sections/.store in=\sectionprefix,
}

\ProcessPgfOptions{/gpppi}

% Hack to combine variable and text without any characters between
\newcommand{\gpppi}{gpppi}
\ProvidesClass{\relativepath\gpppi}[LaTeX class for GPPPI format]


% Page setup
% ========================================

\LoadClass[12pt,draft]{article}

% Page margins
\usepackage[top=25mm,right=25mm,bottom=25mm,left=40mm]{geometry}


% Language setup
% ========================================

\usepackage[english,french]{babel}
\usepackage[T1]{fontenc}
\usepackage{csquotes}


% Font configuration
% ========================================

\usepackage[T1]{fontenc}
\usepackage{xltxtra,fontspec,xunicode}
\setsansfont{Arial}
\setmainfont{Times New Roman}
\newfontfamily{\monofont}{Source Code Pro}
\newfontfamily{\arial}{Arial}
\newfontfamily{\calibri}{Calibri}
\newfontfamily{\timesroman}{Times New Roman}
\newcommand*{\sectionfont}{\arial}


% Section setup
% ========================================

\usepackage{titlesec}

\ifdefined\sectionprefix
\renewcommand{\thesection}{\sectionprefix{} \arabic{section}}
	\renewcommand\thesubsection{\alph{subsection})}
	\renewcommand\thesubsubsection{\arabic{subsubsection}.}
\else
	\renewcommand\thesection{\arabic{section}.}
	\renewcommand\thesubsection{\arabic{section}.\arabic{subsection}.}
	\renewcommand\thesubsubsection{\arabic{section}.\arabic{subsection}.\arabic{subsubsection}.}
\fi

\titleformat*{\section}{\fontsize{14}{16}\bfseries\sffamily}
\titleformat*{\subsection}{\fontsize{12}{24}\bfseries\sffamily}
\titleformat*{\subsubsection}{\fontsize{12}{18}\itshape\sffamily}


% References
% ========================================

\usepackage[square, numbers]{natbib}
\usepackage{IEEEtrantools}
\usepackage{url}
\usepackage[hidelinks]{hyperref}

% Section number in bibliography
\usepackage[numbib]{tocbibind}
\addto{\captionsfrench}{\renewcommand{\bibname}{Références}}


% Math / numbers
% ========================================

% Units and such
\usepackage{siunitx}
\usepackage{amsmath}
\usepackage{mathtools}

% Strikethroughs
\usepackage{cancel}

% Setup common operators and units
\DeclareSIUnit\bit{bit}
\DeclareSIUnit\baud{baud}
\DeclareSIUnit\pixel{px}
\DeclareSIUnit\image{image}
\DeclareMathOperator{\sinc}{sinc}

% cdot for products, slash for fractions
\sisetup{per-mode=symbol,inter-unit-product=\ensuremath{\cdot}}


% Figures
% ========================================

\usepackage[justification=justified,singlelinecheck=false]{caption}
\usepackage{float}
\usepackage{graphicx}
\usepackage{subcaption}

% Code extract "figures"
\usepackage{listings}

% Timing tables
\usepackage{tikz-timing}[2009/05/15]


% Circuits
% ========================================

\usepackage[americanvoltages,fulldiodes,siunitx]{circuitikz}
\usetikzlibrary{shapes,arrows,positioning,calc}


% Tables
% ========================================

\usepackage{tabu}

% Table rules
\usepackage{booktabs}


% Title page
% ========================================

\usepackage{multicol}
\setlength{\columnseprule}{4pt}
\def\columnseprulecolor{\color{black}}

\newcommand*{\gpppititlepage}[6]{%
	\begin{titlepage}
		\centering

		\begin{multicols*}{2}{\calibri
			#1 \par
			#2
			\vfill
			#3
			\vfill
			par \par
			#4
			\vfill
			Présenté à \par
			#5

			\vfill

			Date \par
			#6

			\columnbreak

			\includegraphics[height=0.30\textwidth]{\relativepath/udem-logo.png}\par\vspace{1cm}

			\null \vfill

			{\huge FACULTÉ \\\vspace{5mm} D'INGÉNIERIE}

			\vspace{1cm}

			{\LARGE UNIVERSITÉ \\ DE \vspace{3mm}\\ MONCTON}

			\vspace{1cm}

			{\large Moncton, NB, Canada}

			\vfill
			\vfill

		}\end{multicols*}
	\end{titlepage}
}


% Footer
% ========================================

\usepackage{fancyhdr}
\pagestyle{fancy}

% Remove rule in header
\renewcommand{\headrulewidth}{0pt}

% Remove default header text
\rhead{}
\chead{}
\lhead{}

% Add page number to center of footer
\cfoot{-- \text{ }\thepage\text{ } --}
