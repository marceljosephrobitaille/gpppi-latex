<!-- vim: spelllang=fr
-->

# GPPPI LaTeX

Gabarit LaTeX pour rédiger des documents dans le style de la guide de
préparation des présentations d'un projet d'ingénierie à l'usage des étudiantes
et des étudiants de la faculté d'ingénierie de l'Université de Moncton (GPPPI).

## Utilisation

Il est recommandé d'utiliser le gabarit en tant que "submodule" Git:

```
git init
git submodule add -b master git@gitlab.com:MarcelRobitaille/gpppi-latex.git
```

Par la suite, la classe de document peut être utilise comme suit:

```tex
% !TEX program = xelatex
% !TEX encoding = UTF-8 Unicode

\documentclass[relativepath=./gpppi-latex/]{./gpppi-latex/gpppi}
\usepackage{blindtext}

\begin{document}

\gpppititlepage
	{GELE4132}
	{Asservissements linéaires}
	{Laboratoire 100}
	{Marcel Robitaille (A00181029)}
	{Mohsen Ghribi, Ing, Ph.D}
	{Le 18 octobre 2019}

\section{Exemple de section}
\blindtext{}
\subsection{Exemple de sous section}
\blindtext{}
\subsubsection{Exemple de sous sous section}
\blindtext{}

\end{document}
```

## Fonctions

### Page titres

La classe fournit la commande `\gpppititlepage` qui permet de générer
facilement une page titre dans le style de la faculté.

```tex
\gpppititlepage{<sigle>}{<nom du cours>}{<titre>}{<auteur> (<NI>)}{<prof>}{<date de remise>}
```

### Énumération des sections

Par défaut, le seul préfixe ajouté avant le texte fournit est le numéro de la
(sous) section (par exemple: "1.1.1. Titre de la section").

Pour une énumération plus dans le style d'un devoir, il est possible de fournir
l'option `sections` à la classe:

```tex
\documentclass[relativepath=./gpppi-latex/, sections=Problème]{./gpppi-latex/gpppi}
...
\section{Section}
\subsection{Sous section}
\subsubsection{Sous sous section}
```

Cela produira un document dans le style suivant:

```
Problème 1  Section
a)  Sous section
1.  Sous sous section
```

## Troubleshooting

### Fatal Package fontspec Error: The fontspec package requires either XeTeX or LuaTeX.

La classe utilise les polices du GPPPI (Arial et Calibri). Cela est fait avec
la librairie `fontspec`, qui exige XeTeX ou LuaTeX. Il faut ajouter les lignes
suivantes au-dessus de votre fichier pour utiliser un compilateur XeTeX :

```
% !TEX program = xelatex
% !TEX encoding = UTF-8 Unicode
```

### LaTeX Warning: You have requested document class `./gpppi-latex/gpppi`, but the document class provides `./gpppi-latexgpppi`.

Cette erreur se produit si la `documentclass` utilisée ne correspond pas à ce
que la classe fournit (la classe fournit `<relativepath>gpppi`). Cela peut se
produire si vous utilisez
`\documentclass[relativepath=./gpppi-latex]{./gpppi-latex/gpppi}` au lieu de
`\documentclass[relativepath=./gpppi-latex/]{./gpppi-latex/gpppi}` (notez le
manque de `/` à la fin de `relativepath`).
